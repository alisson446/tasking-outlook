var startServer = require('./src/server/server');
var routeServer = require('./src/server/router');
var { home, authorize, calendar } = require('./src/');

var handle = {};
handle['/'] = home;
handle['/authorize'] = authorize;
handle['/calendar'] = calendar;

startServer(routeServer, handle);
