function getCalendars(client, email, response) {
  client
    .api('/me/calendars')
    .header('X-AnchorMailbox', email)
    .select('name, id')
    .get((err, res) => {
      if (!err) {
        console.log('getCalendars returned ' + res.value.length + ' calendars.');
        response.write('<table><tr><th>Name</th><th>Id</th></tr>');
        res.value.forEach(function(calendar) {
          console.log('  Calendar name: ' + calendar.name);
          response.write('<tr><td>' + calendar.name + 
            '</td><td>' + calendar.id + '</td></tr>');
        });

        response.write('</table>');
        response.end();
      } else {
        console.log(err);
        response.write('<p>Error: ' + err + '</p>');
        response.end();
      }
    });
}

function createCalendar(client, email, response) {
  client
    .api('/me/calendars')
    .header('X-AnchorMailbox', email)
    .post({
      name: 'calendar 3'
    }, (err, res) => {
      if (!err) {
        console.log("calendar created", res.id);
        response.write('<p>calendar created ' + res.id + '</p>');
        response.end();
      } else {
        console.log(err);
        response.write('<p>Error: ' + err + '</p>');
        response.end();
      }
    });
}

function deleteCalendar(client, email, response) {
  const calendarId = 'AQMkADAwATM3ZmYAZS1iZGU4LTI3MjktMDACLTAwCgBGAAADBxlkVmUQ5U_S_sd-0dEAGCkHAHce4gSeqItFv5QYqj1RmrcAAAIBBgAAAHce4gSeqItFv5QYqj1RmrcAAACJ5lf0AAAA'

  client
    .api(`/me/calendars/${calendarId}`)
    .header('X-AnchorMailbox', email)
    .delete((err, res) => {
      if (!err) {
        console.log("calendar deleted");
        response.write('<p>calendar deleted</p>');
        response.end();
      } else {
        console.log(err);
        response.write('<p>Error: ' + err + '</p>');
        response.end();
      }
    });
}

module.exports = {
  getCalendars,
  createCalendar,
  deleteCalendar
}
