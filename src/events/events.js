function getCalendarEvents(client, email, response) {
  client
    .api(`/me/events`)
    .header('X-AnchorMailbox', email)
    .top(10)
    .select('subject,start,end')
    .orderby('start/dateTime DESC')
    .get((err, res) => {
      if (err) {
        console.log('getEvents returned an error: ' + err);
        response.write('<p>ERROR: ' + err + '</p>');
        response.end();
      } else {
        console.log('getEvents returned ' + res.value.length + ' events.');
        response.write('<table><tr><th>Subject</th><th>Start</th><th>End</th><th>Id</th></tr>');
        res.value.forEach(function(event) {
          console.log('  Subject: ' + event.subject);
          response.write('<tr><td>' + event.subject + 
            '</td><td>' + event.start.dateTime.toString() +
            '</td><td>' + event.end.dateTime.toString() + 
            '</td><td>' + event.id +'</td></tr>');
        });

        response.write('</table>');
        response.end();
      }
    });
}

function createCalendarEvent(client, email, response) {
  client
    .api(`/me/events`)
    .header('X-AnchorMailbox', email)
    .post({
      "Subject": "event 1",
      "Start": {
          "DateTime": "2017-06-01T18:00:00",
          "TimeZone": "Pacific Standard Time"
      },
      "End": {
          "DateTime": "2017-06-01T19:00:00",
          "TimeZone": "Pacific Standard Time"
      }
    }, (err, res) => {
      if (!err) {
        console.log("event created");
        response.write('<p>event created</p>');
        response.end();
      } else {
        console.log(err);
        response.write('<p>Error: ' + err + '</p>');
        response.end();
      }
    });
}

function patchCalendarEvent(client, email, response) {

  const eventId = 'AQMkADAwATM3ZmYAZS1iZGU4LTI3MjktMDACLTAwCgBGAAADBxlkVmUQ5U_S_sd-0dEAGCkHAHce4gSeqItFv5QYqj1RmrcAAACK3V3cAAAAdx7iBJ6oi0W-lBiqPVGatwAAAIrdfS8AAAA=';

  const event = {
    "Subject": "event",
    "Start": {
        "DateTime": "2017-06-01T18:00:00",
        "TimeZone": "Pacific Standard Time"
    },
    "End": {
        "DateTime": "2017-06-01T19:00:00",
        "TimeZone": "Pacific Standard Time"
    }
  };

  client
    .api(`/me/events/${eventId}`)
    .header('X-AnchorMailbox', email)
    .patch(event, (err, event) => {
      if (!err) {
        console.log('Event updated');
        response.write('<p>event updated</p>');
        response.end();
      } else {
        console.log(err);
        response.write('<p>Error: ' + err + '</p>');
        response.end();
      }
    });

}

module.exports = {
  getCalendarEvents,
  createCalendarEvent,
  patchCalendarEvent
}
