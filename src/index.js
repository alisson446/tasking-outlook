var url = require('url');
var microsoftGraph = require("@microsoft/microsoft-graph-client");
var { getAuthUrl, getTokenFromCode } = require('./outlookAuth/authHelper');
var { tokenReceived, getAccessToken, getValueFromCookie } = require('./outlookAuth/authorize');
var { getCalendarEvents, createCalendarEvent, patchCalendarEvent } = require('./events/events');
var { getCalendars, createCalendar, deleteCalendar } = require('./calendar/calendar');

function home(response, request) {
  console.log('Request handler \'home\' was called.');
  response.writeHead(200, {'Content-Type': 'text/html'});
  response.write('<p>Please <a href="' + getAuthUrl() + '">sign in</a> with your Office 365 or Outlook.com account.</p>');
  response.end();
}

function authorize(response, request) {
  console.log('Request handler \'authorize\' was called.');

  // The authorization code is passed as a query parameter
  var url_parts = url.parse(request.url, true);
  var code = url_parts.query.code;
  
  getTokenFromCode(code, tokenReceived, response);
}

function calendar(response, request) {
  getAccessToken(request, response, function(error, token) {
    console.log('Token found in cookie: ', token);
    var email = getValueFromCookie('node-tutorial-email', request.headers.cookie);
    console.log('Email found in cookie: ', email);
    if (token) {
      response.writeHead(200, {'Content-Type': 'text/html'});
      response.write('<div><h1>Your calendar</h1></div>');

      // Create a Graph client
      var client = microsoftGraph.Client.init({
        authProvider: (done) => {
          // Just return the token
          done(null, token);
        }
      });

      getCalendars(client, email, response);
      
    } else {
      response.writeHead(200, {'Content-Type': 'text/html'});
      response.write('<p> No token found in cookie!</p>');
      response.end();
    }
  });
}

module.exports = {
  home,
  authorize,
  calendar
}
