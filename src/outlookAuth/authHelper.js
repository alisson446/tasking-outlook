const credentials = require('../../outlook_credentials.json');
var oauth2 = require('simple-oauth2').create(credentials);

var redirectUri = 'https://us-central1-tasking-dev-ao.cloudfunctions.net/calendarIntegrations';

var scopes = [
  'openid',
  'offline_access',
  'User.Read',
  'Calendars.readwrite'
];

function getAuthUrl() {
  var returnVal = oauth2.authorizationCode.authorizeURL({
    redirect_uri: redirectUri,
    scope: scopes.join(' '),
    state: 'outlookCalendar'
  });
  console.log('Generated auth url: ' + returnVal);
  return returnVal;
}

function getTokenFromCode(auth_code, callback, response) {
  var token;
  oauth2.authorizationCode.getToken({
    code: auth_code,
    redirect_uri: redirectUri,
    scope: scopes.join(' ')
  }, function (error, result) {
    if (error) {
      console.log('Access token error: ', error.message);
      callback(response, error, null);
    } else {
      token = oauth2.accessToken.create(result);
      console.log('Token created: ', token.token);
      callback(response, null, token);
    }
  });
}

function refreshAccessToken(refreshToken, callback) {
  var tokenObj = oauth2.accessToken.create({refresh_token: refreshToken});
  tokenObj.refresh(callback);
}

module.exports = {
  getAuthUrl,
  getTokenFromCode,
  refreshAccessToken
}
