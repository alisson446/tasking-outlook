var microsoftGraph = require("@microsoft/microsoft-graph-client");
var { refreshAccessToken } = require('./authHelper');

function getUserEmail(token, callback) {
  var client = microsoftGraph.Client.init({
    authProvider: (done) => {
      done(null, token);
    }
  });

  client
    .api('/me')
    .get((err, res) => {
      if (err) {
        callback(err, null);
      } else {
        callback(null, res.userPrincipalName);
      }
    });
}

function tokenReceived(response, error, token) {
  if (error) {
    console.log('Access token error: ', error.message);
    response.writeHead(200, {'Content-Type': 'text/html'});
    response.write('<p>ERROR: ' + error + '</p>');
    response.end();
  } else {
    getUserEmail(token.token.access_token, function(error, email){
      if (error) {
        console.log('getUserEmail returned an error: ' + error);
        response.write('<p>ERROR: ' + error + '</p>');
        response.end();
      } else if (email) {
        var cookies = [
        	'node-tutorial-token=' + token.token.access_token + ';Max-Age=4000',
					'node-tutorial-refresh-token=' + token.token.refresh_token + ';Max-Age=4000',
					'node-tutorial-token-expires=' + token.token.expires_at.getTime() + ';Max-Age=4000',
					'node-tutorial-email=' + email + ';Max-Age=4000'
        ];
        response.setHeader('Set-Cookie', cookies);
        response.writeHead(302, {'Location': 'http://localhost:8000/calendar'});
        response.end();
      }
    }); 
  }
}

function getValueFromCookie(valueName, cookie) {
  if (cookie.indexOf(valueName) !== -1) {
    var start = cookie.indexOf(valueName) + valueName.length + 1;
    var end = cookie.indexOf(';', start);
    end = end === -1 ? cookie.length : end;
    return cookie.substring(start, end);
  }
}

function getAccessToken(request, response, callback) {
  var expiration = new Date(parseFloat(getValueFromCookie('node-tutorial-token-expires', request.headers.cookie)));

  if (expiration <= new Date()) {
    console.log('TOKEN EXPIRED, REFRESHING');
    var refresh_token = getValueFromCookie('node-tutorial-refresh-token', request.headers.cookie);
    authHelper.refreshAccessToken(refresh_token, function(error, newToken){
      if (error) {
        callback(error, null);
      } else if (newToken) {
        var cookies = [
        	'node-tutorial-token=' + newToken.token.access_token + ';Max-Age=4000',
					'node-tutorial-refresh-token=' + newToken.token.refresh_token + ';Max-Age=4000',
					'node-tutorial-token-expires=' + newToken.token.expires_at.getTime() + ';Max-Age=4000'
        ];
        response.setHeader('Set-Cookie', cookies);
        callback(null, newToken.token.access_token);
      }
    });
  } else {
    // Return cached token
    var access_token = getValueFromCookie('node-tutorial-token', request.headers.cookie);
    callback(null, access_token);
  }
}

module.exports = {
	tokenReceived,
	getValueFromCookie,
	getAccessToken
}
